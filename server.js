//import express module
const exp=require("express");

//get express object
const app=exp();

//request forwarding logic
//import user and admin apis
const userApp=require('./APIS/userApi');
const adminApp=require('./APIS/adminApi');

//forward req obj to apis
app.use('/user',userApp);
app.use('/admin',adminApp);

//connect to angular app
const path=require("path");
app.use(exp.static(path.join(__dirname,"./dist/Angular")))

//assign port number
const port=9000;
app.listen(port,()=>{
    console.log(`server is running on ${port}`)
})

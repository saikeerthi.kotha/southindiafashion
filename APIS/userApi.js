//import express module
const exp=require("express");
//create miniApp
const userApp=exp.Router();
//import mongodb
const mc=require("mongodb").MongoClient;
//import bcrypt
const bcrypt=require("bcrypt");
//import jsonwebtoken
const jwt=require("jsonwebtoken");
//import twilio
const twilio=require("twilio");
const { error } = require("protractor");

//database url
const dbUrl="mongodb+srv://saikeerthi:sairam009@cluster0-gib8t.mongodb.net/<dbname>?retryWrites=true&w=majority";

//connect to db using url
var dbo;
mc.connect(dbUrl,{useNewUrlParser:true,useUnifiedTopology:true},(error,client)=>{
    if(error){
        console.log("error in db connection",error)
    }
    else{
        //get database object
        dbo=client.db("kutti");
        console.log("connected to db");
    }
})

//POST req for country
//parse req obj into java script obj
userApp.use(exp.json())
userApp.post('/country',(req,res)=>{
    dbo.collection("country").insertOne(req.body,(error,result)=>{
        if(error){
            console.log("error in saving country code",error)
        }
        else{
            res.send({message:"saved successfully"})
        }
    })
})

//GET req in registration for country code
userApp.get('/countrycode',(req,res)=>{
    dbo.collection("country").find().toArray((error,country)=>{
        if(error){
            console.log("error in finding countries",error)
        }
        else{
            res.send({message:country})
        }
    })
})

userApp.post('/registration',(req,res)=>{
    console.log(req.body)
    dbo.collection("user").findOne({email:req.body.email},(error,userObj)=>{
        if(error){
            console.log("error in finding registration",error);
        }
        else if(userObj==null){
            console.log(userObj)
            console.log(req.body.password)
            //hash password
            let hashedpassword=bcrypt.hashSync(req.body.password,6);
            //replacing text pw with hashed passord
            req.body.password=hashedpassword
            dbo.collection("user").insertOne(req.body,(error,success)=>{
                if(error){
                    console.log("error in registration")
                }
                else{
                    res.send({message:"user created"})
                }
            })
        }
        else{
            res.send({message:"user existed"})
        }
    })
})


//Login
userApp.post('/login',(req,res)=>{
    dbo.collection("user").findOne({email:req.body.email},(error,userObj)=>{
        if(error){
            console.log("error in finding login",error);
        }
        else if(userObj==null){
            res.send({message:"user not existed"})
        }
        else{
            bcrypt.compare(req.body.password,userObj.password,(error,match)=>{
                if(error){
                    console.log("error in pw matching",error)
                }
                else if(match==false){
                    res.send({message:"invalid password"})
                }
                else{
                    //password matched then signin and create token to client
                    jwt.sign({email:userObj.email},'hiiii',{expiresIn:60},(error,signedToken)=>{
                        if(error){
                            console.log("error in creating token",error)
                        }
                        else{
                            res.send({message:"logged in",token:signedToken,obj:userObj,username:userObj.username})
                        }
                    })
                }
            })
        }
    })
})

//Mobile number verification
const accountSid = 'AC45c873c5b57ac56d8ca81d92b06d3a2e';
const authToken = '381251527039bdc6fde825aeff457ea0';
const client = require('twilio')(accountSid, authToken);

userApp.post('/mobilenumberverification',(req,res)=>{
    console.log(req.body)
    dbo.collection("user").findOne({mobilenumber:req.body.mobilenumber},(error,userObj)=>{
        if(error){
            console.log("error in verifying mobile number",error)
        }
        else if(userObj==null){
            res.send({message:"number not existed"})
        }
        else{
            console.log(userObj)
            var OTP=Math.floor(Math.random()*99999)+1111;
            console.log(OTP)
            client.messages.create({
                body:`Your verification code is ${OTP}`,
                from:'+14159094341',
                to:userObj.mobilenumber,
            })
            dbo.collection("otp").insertOne({otp:OTP,email:userObj.email,OTPGeneratedTime:new Date().getTime()+15000},(error,success)=>{
                if(error){
                    console.log("error in saving otp",error)
                }
                else{
                    res.send({message:"user found",otp:OTP,userObj})
                }
            })
            // .then((message)=>{
            //     console.log(message.sid)
                
            // })
        }
    })
})

//OTP Verification
// userApp.post('/verifyotp',(req,res)=>{
//     dbo.collection("otp").findOne({email:req.body.email},(error,userObj)=>{
//         console.log(userObj)
//         if(error){
//             console.log(error)
//         }
//         else if(userObj==null){
//             res.send({message:"no"})
//         }
//         else{
//             dbo.collection("otp").findOne({Otp:req.body.otp},(error,result)=>{
//                 console.log(result)
//                 if(error){
//                     console.log(error)
//                 }
//                 else{
//                     console.log(result)
//                 }
//                 // else if(req.body.otp!==OTP){
//                 //     res.send({message:"invalid otp"})
//                 // }
//                 // else if(result.OTPGeneratedTime < req.body.currenTime){
//                 // }
//             })
//         }
//     })
// })

//export miniApp
module.exports=userApp;




userApp.post("/verifyotp",(req,res)=>{
    console.log(req.body)
    var data=parseInt(req.body.otp)
    console.log(data)
    req.body.timestamp=new Date().getTime()
    dbo.collection("otp").findOne({email:req.body.email,otp:data},(err,resultDataFrom)=>{
        if(err){console.log(err)}
        else if(resultDataFrom!==null){
             console.log(resultDataFrom)
          if(req.body.timestamp<resultDataFrom.OTPGeneratedTime){    
              
              res.json({message:"otp successfully verified",id:resultDataFrom.id})    
        }
         else{
             res.json({message:"session time was experied"})     
        }  
    }
     })
    })
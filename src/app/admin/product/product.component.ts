import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private hc:HttpClient) { }

  ngOnInit(): void {
  }
  
  //preview the image
  file:File

  imageUrl: string | ArrayBuffer="https://bulma.io/images/placeholders/480*480.png";
  selectImage(f:File){
    console.log("file data is:",f)
    this.file=f;
      
    //create obj on FileReader
    const reader=new FileReader()

    //read data from file
    reader.readAsDataURL(this.file)
    reader.onload=()=>{
      this.imageUrl=reader.result;
    }
  }
  
  formData(data){
    console.log(data)
    //take object from FormData

    let fd=new FormData()
    fd.append("photo",this.file);
    fd.append("userObj",data);
    console.log(fd);
    this.hc.post<object>('/admin/images',fd).subscribe((response)=>{
      console.log(response)
    })
    
  }
}

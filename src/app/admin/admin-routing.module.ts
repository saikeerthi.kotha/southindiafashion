import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { ProductComponent } from './product/product.component';


const routes: Routes = [
  {path:"admindashboard",component:AdmindashboardComponent,children:[{path:"product",component:ProductComponent}]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

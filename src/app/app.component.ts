import { Component, Output } from '@angular/core';
import { EventEmitter } from 'events';
import { LoginService } from './login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular';
  constructor(public ls:LoginService){}
  
}

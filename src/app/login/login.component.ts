import { Component, OnInit, Input, Output ,EventEmitter } from '@angular/core';
import { DataService } from '../data.service';
import { LoginService } from '../login.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  // @Input() parentToChild:[];
  constructor(private ls:LoginService,private ds:DataService,private hc:HttpClient,private router:Router) { }
  data:any;
  ngOnInit(): void {
    setTimeout(()=>{
      this.ls.logout(),0
    })
    //Country code dummy api
    // this.hc.get<object>('https://gist.githubusercontent.com/Goles/3196253/raw/9ca4e7e62ea5ad935bb3580dc0a07d9df033b451/CountryCodes.json').subscribe((data)=>{
    //   console.log(data)
    //   this.data=data
    // })
  }

  // datato(i){
  //   console.log(i)
  //   i.countrycode=this.data
  //   this.hc.post<object>('/user/country',i).subscribe((response)=>{
  //     console.log(response)
  //   })  
  // }
  // @Output() myEvent= new EventEmitter();

  // dataToChild:any[]=[]
  // d:any;
  // e:any;
  // data(v){
  //   console.log(v);
  //   this.d=v;
  //   console.log(this.d)
  //   this.ds.dataShare=this.dataToChild.push(this.d);
  // }

  //login
  role:string;
  loginData(data){
    console.log(data)
    this.ls.login(data).subscribe((response)=>{
      console.log(response);
      if(response["message"]=="user not existed"){
        alert("invalid username")
      }
      if(response["message"]=="invalid password"){
        alert("invalid password")
      }
      else{
        //store token in local storage
        localStorage.setItem("token",response["token"]);
        //updating user login status
        this.ls.userstatus=true;
        //updating username
        this.ls.username=response["username"]
        console.log(response["username"]);

        //navigating to userdashboard
        if(response["obj"]["role"]=="user"){
          this.router.navigate(["../userdashboard"])
        }
        else{
          this.router.navigate(['../admindashboard'])
        }
      }
    })
  }
  forgotPassword(){
    console.log("hi")
    this.router.navigate(['/otp'])
  }
}

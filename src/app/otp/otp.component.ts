import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  constructor(private hc:HttpClient) { }

  ngOnInit(): void {
  }
  display:boolean=true;
  mn:number;
  userData:any;
  changeStatus(number){
    console.log(number)
    this.display=false;
    this.mn=number.mobilenumber;
    //send mobile number to db for verification
    this.hc.post<object>('/user/mobilenumberverification',number).subscribe((response)=>{
      console.log(response)
      this.userData=response["userObj"]
      console.log(this.userData)
    })
  }
  otpVerify(data){
    console.log(data)
    data.email=this.userData.email
    console.log(data)
    this.hc.post<object>('/user/verifyotp',data).subscribe((response)=>{
      console.log(response);
    })
  }
}

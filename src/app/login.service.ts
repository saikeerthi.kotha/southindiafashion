import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private hc:HttpClient) { }
  username:string;
  userstatus:boolean=false;
  login(data):Observable<object>
  {
    return this.hc.post<object>('/user/login',data);
  }
  logout(){
    localStorage.removeItem("token");
    this.userstatus=false;
  }
}

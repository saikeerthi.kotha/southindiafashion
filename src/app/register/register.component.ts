import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { LoginService } from '../login.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private ls:LoginService,private hc:HttpClient) { }

  data:any
  country:any;
  countrycode:any;
  ngOnInit(): void {
    // this.data=this.ds.data()
    this.hc.get<object>('/user/countrycode').subscribe(response=>{
      console.log(response)
      this.country=response["message"]
      console.log(this.country)
      for(var i of this.country){
        console.log(i.countrycode)
        this.countrycode=i.countrycode
      }
    })
  }

  // d:any;
  // childToParent:any=[];
  // edit(j){
  //   // this.d=this.parentToChild.splice(j,1);
  //   // console.log(this.d[0])
  //   // // this.childToParent.push(this.d[0]);
  //   // // console.log(this.childToParent);
  //   // this.myEvent.emit(this.d[0])
  // }

  //Registratation form data
  formData(data){
    console.log(data)
    //comparing password
    if(data.password1==data.password2){
      //adding username with firstname and lastname
      data.username=data.firstname+data.lastname;
      data.password=data.password1
      delete data.password1;
      delete data.password2;
      //adding role to object
      data.role="user"
      console.log(data)
      this.hc.post<object>('/user/registration',data).subscribe((response)=>{
        console.log(response);
      })
    }
    else{
      alert("wrong password")
    }
  }
}
